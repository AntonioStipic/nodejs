var express = require("express");
var mon = require("mon");
var bodyParser = require("body-parser");
var jwt = require("jsonwebtoken");
var mysql = require("mysql");

var app = express();
app.use(bodyParser.json());
app.use(express.static(__dirname + "/static"))

var users = [];
var secret = "neka-fraza";

var connection = mysql.createConnection({
	host: "localhost",
	user: "root",
	password: "",
	database: "nodejs"
});

connection.connect(function(err) {
	if (err){
		console.log("Error connecting to database", err);
	} else {
		console.log("Successfully connected to database");
	}
})

var query = "SELECT * FROM users;";
connection.query(query, function(error, result) {
	if (error) {
		console.log("Mysql query error", error);
	} else {
		console.log("users:", result);
	}
});

//connection.end();

app.listen(8080, function () {
	console.log('Listening on port 8080!');
});

app.get('/', function(request, response){
	//response.send(request);
	console.log("REQUEST:", request);
	response.send("This is GET method!");
});

//////////////////////////////////

app.post("/auth", function (req, res){
	var user = getUser(req.body.username, req.body.password);
	
	var status = 401;
	var response = {"success": false};
	
	if (user != null) {
		var token = jwt.sign(user, secret, {
			expiresIn: "1m"
		});
		response.success = true;
		response.token = token;
		status = 200;
	}
	res.status(status).json(response);
});

app.get("/login", function (req, res){
	res.sendFile(__dirname + "/login.html");
});

app.get("/register", function (req, res){
	res.sendFile(__dirname + "/register.html");
});

app.post("/register", function (req, res){
	var username = req.body.username;
	var password = req.body.password;
	var success = false;

	if (!userExists(username)) {
		var user = {"username": username, "password": password};
		users.push(user);
		success = true;
		console.log("user", user);
	}
	res.json({"success": success});
});

//////////////////////////////////

var apiRoute = express.Router();

apiRoute.use(function (req, res, next) {
	var token = req.query.token || req.headers["x-auth-token"];
	console.log(req.headers);
	if (token) {
		jwt.verify(token, secret, function(err, payload) {
			if (err) {
				return res.status(401).json({success: false, message: "Krivi token"});
			} else {
				next();
			}
		});
	} else {
		return res.status(401).json({success: false, message: "Fali token"});
	}
});

apiRoute.get("/users", function (req, res) {
	res.status(200).json(users);
});

app.use("/api", apiRoute);

//-------------------------------------------------------------------

function getUser(username, password) {
	for (var i = 0; i < users.length; i++) {
		if (users[i].username == username && users[i].password == password) {
			return users[i];
		}
	}
	return null;
}

function verifyLogin(username, password){
	for (var i = 0; i < users.length; i++) {
		if (users[i].username == username && users[i].password == password) {
			return true;
		}
	}
	return false;
}

function userExists(username) {
	for (var i = 0; i < users.length; i++) {
		if (users[i].username == username) {
			return true;
		}
	}
	return false;
}