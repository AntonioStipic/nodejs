var app = angular.module("projekt");

app.controller("LoginController",
function($rootScope, $scope, $http) {
	this.username ="prazno";
	this.password = "prazno";
	var self = this;
	$rootScope.loginSuccess = false;

	this.send = function(username, password) {
		self.username = username;
		self.password = password;

		var data = {username:username, password:password};

		$http({
			data: data,
			method: "POST",
			url: "/auth"
		}).then(function successCallback(response){
			console.log("response", response);
			$rootScope.loginSuccess = true;
			$rootScope.token = response.data.token;
		}), function errorCallback(response){
			console.log("GRESKA");
			$rootScope.registerSuccess = false;
		}
	}
});
